package controler;

import model.ChessGame;
import model.Coord;
import tools.Commands.CommandeMove;
import tools.Commands.ICommande;
import tools.Commands.Invocateur;
import tools.MyObserver;

public class ChessGameControler implements ChessGameControlers {
    private ChessGame model;
    private Invocateur<ICommande> telecommande;

    public ChessGameControler(ChessGame model) {
        this.model = model;
        telecommande = new Invocateur<>();
    }

    @Override
    public boolean move(Coord initCoord, Coord finalCoord) {
        ICommande ordre = new CommandeMove(model, initCoord.x, initCoord.y, finalCoord.x, finalCoord.y);
        return telecommande.doIt(ordre);
    }

    @Override
    public String getMessage() {
        return model.getMessage();
    }

    @Override
    public boolean isEnd() {
        return model.isEnd();
    }

    @Override
    public boolean isPlayerOK(Coord initCoord) {
        return (model.getColorCurrentPlayer().equals(model.getPieceColor(initCoord.x, initCoord.y)));
    }

    public void undoMove() {
        telecommande.undo();
    }

    public void redoMove() {
        telecommande.redo();
    }

}
