package tools.Commands;

import model.ChessGame;

public class CommandeMove implements ICommande {
    private ChessGame recepteur;
    private int xInit;
    private int yInit;
    private int xFinal;
    private int yFinal;

    public CommandeMove(ChessGame recepteur, int xInit, int yInit, int xFinal, int yFinal) {
        this.recepteur = recepteur;
        this.xInit = xInit;
        this.yInit = yInit;
        this.xFinal = xFinal;
        this.yFinal = yFinal;
    }

    @Override
    public boolean move() {
        return this.recepteur.move(xInit, yInit, xFinal, yFinal);
    }

    @Override
    public boolean unMove() {
        return this.recepteur.unMove(xFinal, yFinal, xInit, yInit);
    }



}
