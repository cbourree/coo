package tools.Commands;

import java.util.List;
import java.util.Stack;

public class Invocateur<C extends ICommande> {

    private Stack<ICommande> commandesDone;
    private Stack<ICommande> commandesUndone;

    public Invocateur() {
        this.commandesDone = new Stack<ICommande>();
        this.commandesUndone = new Stack<ICommande>();
    }

    public boolean doIt(C cmd){
        if (cmd.move()) {
            commandesDone.push(cmd);
            return true;
        } else {
            return false;
        }
    }

    public void undo() {
        if (!commandesDone.empty()){
            ICommande cmd = commandesDone.pop();
            cmd.unMove();
            commandesUndone.push(cmd);
        } else {
            System.out.println("Aucune cmd à undo\n");
        }
    }

    public void redo() {
        if (!commandesUndone.empty()){
            ICommande cmd = commandesUndone.pop();
            cmd.move();
            commandesDone.push(cmd);
        } else {
            System.out.println("Aucune cmd à redo\n");
        }
    }
}
