package tools.ex_factories;

import model.strategies.*;
import java.util.HashMap;
import java.util.Map;

public class TempeteFactory extends ModeJeuFactory {
    private Map<Integer, MoveStrategy> tempete;
    public static TempeteFactory INSTANCE = new TempeteFactory();

    private TempeteFactory() {
        tempete = new HashMap<Integer, MoveStrategy>();
        this.tempete.put(0, TourMoveStrat.getINSTANCE());
        this.tempete.put(1, CavalierMoveStrat.getINSTANCE());
        this.tempete.put(2, FouMoveStrat.getINSTANCE());
        this.tempete.put(5, FouMoveStrat.getINSTANCE());
        this.tempete.put(6, CavalierMoveStrat.getINSTANCE());
        this.tempete.put(7, TourMoveStrat.getINSTANCE());
    }

    @Override
    public MoveStrategy getMoveStrategy(String pieceName, int x) {
        MoveStrategy ret;// = PionBlancMoveStrat.getINSTANCE(); //default
        if (x==3 || x==4){
            ret = this.getStratFromPiece(pieceName);
        }else{
            ret = tempete.get(x);
        }
        return ret;
    }
}
