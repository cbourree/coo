package tools.ex_factories;

import model.strategies.*;
import tools.Introspection;

import java.lang.reflect.InvocationTargetException;

public abstract class ModeJeuFactory implements InterfaceModeFactory {
    public abstract MoveStrategy getMoveStrategy(String pieceName, int x);

    public MoveStrategy getStratFromPiece(String pieceName) {
        MoveStrategy ret = null;// = getINSTANCE(); //default
        try {
            ret = (MoveStrategy) Introspection.invokeStatic("model.strategies."+pieceName + "MoveStrat",null,"getINSTANCE");
            /*Class<?> clazz = Class.forName();
            Method m = clazz.getMethod("getINSTANCE", null);
            ret = (MoveStrategy) m.invoke(null, null);*/
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }
}
