package tools.ex_factories;

import model.strategies.*;

public class ClassicFactory extends ModeJeuFactory {
    private ClassicFactory(){}
    public static ClassicFactory INSTANCE = new ClassicFactory();

    @Override
    public MoveStrategy getMoveStrategy(String pieceName, int x) {
            return this.getStratFromPiece(pieceName);
    }
}
