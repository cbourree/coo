package tools.ex_factories;

import model.strategies.MoveStrategy;

public interface InterfaceModeFactory {
    public abstract MoveStrategy getMoveStrategy(String pieceName, int x);
    public MoveStrategy getStratFromPiece(String pieceName);
}
