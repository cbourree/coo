package tools;

import model.PieceIHMs;

import java.util.List;

public interface MyObserver {
    public void update(List<PieceIHMs> pieceIHMs);
}
