package tools;

import model.PieceIHMs;

import java.util.List;

public interface MyObservable {
    public void attach(MyObserver o);
    public void detach(MyObserver o);
    public void MyNotify(List<PieceIHMs> l);
}
