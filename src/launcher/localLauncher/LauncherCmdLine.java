package launcher.localLauncher;

import java.util.Observer;

import controler.ChessGameControler;
import model.ChessGame;
import model.Echiquier;
import vue.ChessGameCmdLine;
import controler.ChessGameControlers;



/**
 * @author francoise.perrin
 * Lance l'exécution d'un jeu d'échec en factory console.
 */
public class LauncherCmdLine {
	
	public static void main(String[] args) {		
		
		ChessGame model;
		ChessGameControlers controler;	
		ChessGameCmdLine vue;
		
		model = new ChessGame();
		controler = new ChessGameControler(model);

		//Echiquier e = new Echiquier();
		//new ChessGameCmdLine(e);
		
		vue = new ChessGameCmdLine(controler);

		model.attach(vue);

		vue.go();
	}

}
