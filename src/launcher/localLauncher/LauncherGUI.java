package launcher.localLauncher;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

import controler.ChessGameControler;
import controler.ChessGameControlers;
import model.ChessGame;
import tools.ex_factories.ClassicFactory;
import tools.ex_factories.ModeJeuFactoryProvider;
import tools.MyObserver;
import tools.ex_factories.TempeteFactory;
import vue.ChessGameGUI;


/**
 * @author francoise.perrin
 * Lance l'exécution d'un jeu d'échec en factory graphique.
 * La vue (ChessGameGUI) observe le modèle (ChessGame)
 * les échanges passent par le contrôleur (ChessGameControlers)
 * 
 */
public class LauncherGUI {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		/**/
		JFrame frame2 = new JFrame();
		JPanel panel = new JPanel();
		JLabel label = new JLabel("Veuillez faire un choix");
		JButton button1 = new JButton("Normal");
		JButton button2 = new JButton("Tempête");
		panel.add(label);
		panel.add(button1);
		panel.add(button2);
		frame2.setContentPane(panel);
		frame2.pack();
		frame2.setVisible(true);
		/**/

		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ModeJeuFactoryProvider.factory = ClassicFactory.INSTANCE;
				System.out.println("Normal");
				frame2.setVisible(false);
				startGUI();
			}
		});

		button2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ModeJeuFactoryProvider.factory = TempeteFactory.INSTANCE;
				System.out.println("Tempête");
				frame2.setVisible(false);
				startGUI();
			}
		});


	}

	public static void startGUI() {
		ChessGame chessGame;
		ChessGameControlers chessGameControler;
		JFrame frame;
		Dimension dim;

		dim = new Dimension(700, 700);

		chessGame = new ChessGame();
		chessGameControler = new ChessGameControler(chessGame);

		frame = new ChessGameGUI("Jeu d'échec", chessGameControler,  dim);
		//chessGame.addObserver((Observer) frame);
		chessGame.attach((MyObserver) frame);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocation(600, 10);
		frame.setPreferredSize(dim);
		frame.pack();
		frame.setVisible(true);

		frame.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {
				if ((e.getKeyCode() == KeyEvent.VK_Z) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
					System.out.println("Undo !!");
					chessGameControler.undoMove();
				} else if ((e.getKeyCode() == KeyEvent.VK_Y) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
					System.out.println("Redo !!");
					chessGameControler.redoMove();
				}
			}
		});
	}
}
