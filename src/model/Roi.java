package model;


import model.strategies.RoiMoveStrat;

/**
 * @author francoise.perrin
 * * Inspiration Jacques SARAYDARYAN, Adrien GUENARD
 */
public class Roi extends AbstractPiece {
	

	/**
	 * @param name
	 * @param couleur_de_piece
	 * @param coord
	 */
	public Roi( Couleur couleur_de_piece, Coord coord) {
		super(couleur_de_piece, coord);
	}
}
