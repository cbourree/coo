package model;

public class PieceIHM implements PieceIHMs {
    private Pieces p;
    private String img;

    public PieceIHM(Pieces p, String img) {
        this.p = p;
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public int getX(){
        return p.getX();
    }

    public int getY(){
        return p.getY();
    }

    public String getName(){
        return p.getName();
    }

    @Override
    public Couleur getCouleur() {
        return p.getCouleur();
    }
}
