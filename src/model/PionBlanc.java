package model;

import model.strategies.PionBlancMoveStrat;

public class PionBlanc extends Pion {
    /**
     * @param couleur_de_piece
     * @param coord
     */
    public PionBlanc(Couleur couleur_de_piece, Coord coord) {
        super(couleur_de_piece, coord);
    }
}