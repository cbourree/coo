package model;

import tools.MyObservable;
import tools.MyObserver;

import java.util.List;

public class ChessGame extends ObservableClass implements BoardGames {
    private Echiquier echiquier;
    //private subjectState

    public ChessGame() {
        super();
        this.echiquier = new Echiquier();
    }

    @Override
    public boolean move(int xInit, int yInit, int xFinal, int yFinal) {
        boolean b = false;
        if (echiquier.isMoveOk(xInit, yInit, xFinal, yFinal)){
            b = echiquier.move(xInit, yInit, xFinal, yFinal);
        }
        if (b){
            echiquier.switchJoueur();
        }
        this.MyNotify(echiquier.getPieceIHMs());
        return b;
    }

    public boolean unMove(int xInit, int yInit, int xFinal, int yFinal) {
        echiquier.switchJoueur();
        echiquier.move(xInit, yInit, xFinal, yFinal);
        this.MyNotify(echiquier.getPieceIHMs());
        return true;
    }

    @Override
    public boolean isEnd() {
        return echiquier.isEnd();
    }

    @Override
    public String getMessage() {
        return echiquier.getMessage();
    }

    @Override
    public Couleur getColorCurrentPlayer() {
        return echiquier.getColorCurrentPlayer();
    }

    @Override
    public Couleur getPieceColor(int x, int y) {
        return echiquier.getPieceColor(x, y);
    }

}
