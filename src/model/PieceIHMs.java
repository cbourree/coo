package model;

public interface PieceIHMs {

    public String getImg();

    public int getX();

    public int getY();

    public String getName();

    public Couleur getCouleur();
}
