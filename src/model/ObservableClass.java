package model;

import tools.MyObservable;
import tools.MyObserver;

import java.util.ArrayList;
import java.util.List;

public abstract class ObservableClass implements MyObservable {
    protected List<MyObserver> observers;

    public ObservableClass() {
        this.observers = new ArrayList<MyObserver>();
    }

    @Override
    public void attach(MyObserver o){
        observers.add(o);
    }

    @Override
    public void detach(MyObserver o) {
        observers.remove(o);
    }

    @Override //override in chessgame for piecesIHM
    public void MyNotify(List<PieceIHMs> l) {
        for (MyObserver o : observers) {
            o.update(l);
        }
    }
}
