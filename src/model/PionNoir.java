package model;

import model.strategies.PionNoirMoveStrat;

public class PionNoir extends Pion {
    /**
     * @param couleur_de_piece
     * @param coord
     */
    public PionNoir(Couleur couleur_de_piece, Coord coord) {
        super(couleur_de_piece, coord);
    }
}
