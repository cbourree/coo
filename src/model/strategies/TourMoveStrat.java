package model.strategies;

public class TourMoveStrat implements MoveStrategy {
    private TourMoveStrat(){}
    private static TourMoveStrat INSTANCE = new TourMoveStrat();

    public static TourMoveStrat getINSTANCE(){
        return  INSTANCE;
    }

    @Override
    public boolean isMoveOk(int xInit, int yInit, int xFinal, int yFinal, boolean isCatchOk, boolean isCastlingPossible, boolean premierCoup) {
        boolean ret = false;

        if ((yFinal == yInit) || (xFinal == xInit)) {
            ret = true;
        }

        return ret;
    }
}
