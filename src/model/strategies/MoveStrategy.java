package model.strategies;

public interface MoveStrategy {
    public boolean isMoveOk(int xInit, int yInit, int xFinal, int yFinal, boolean isCatchOk,
                            boolean isCastlingPossible, boolean premierCoup);
}
