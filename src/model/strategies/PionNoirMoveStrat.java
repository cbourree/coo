package model.strategies;

public class PionNoirMoveStrat extends PionMoveStrat {
    private PionNoirMoveStrat(){}
    private static PionNoirMoveStrat INSTANCE = new PionNoirMoveStrat();

    public static PionNoirMoveStrat getINSTANCE(){
        return  INSTANCE;
    }

    @Override
    public boolean isDepDiagoOK(int xInit, int yInit, int xFinal, int yFinal) {
        if ((yFinal == yInit-1 && xFinal == xInit+1)
                || (yFinal == xInit-1 && xFinal == xInit-1)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isSensVerticalOk(int yInit, int yFinal) {
        return (yFinal - yInit > 0);
    }
}
