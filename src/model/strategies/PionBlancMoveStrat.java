package model.strategies;

public class PionBlancMoveStrat extends PionMoveStrat {
    private PionBlancMoveStrat(){}
    private static PionBlancMoveStrat INSTANCE = new PionBlancMoveStrat();

    public static PionBlancMoveStrat getINSTANCE(){
        return  INSTANCE;
    }

    @Override
    public boolean isDepDiagoOK(int xInit, int yInit, int xFinal, int yFinal) {
        if ((yFinal == yInit-1 && xFinal == xInit+1)
                || (yFinal == yInit-1 && xFinal == xInit-1)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isSensVerticalOk(int yInit, int yFinal) {
        return (yFinal - yInit < 0);
    }
}
