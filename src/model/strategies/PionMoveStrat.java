package model.strategies;

public abstract class PionMoveStrat implements MoveStrategy {
    @Override
    public boolean isMoveOk(int xInit, int yInit, int xFinal, int yFinal, boolean isCatchOk,
                            boolean isCastlingPossible, boolean premierCoup) {
        boolean ret = false;

        // Déplacement vertical
        if (!isCatchOk && !isCastlingPossible){

            if ((xFinal == xInit)
                    && (Math.abs(yFinal - yInit) <= 1 ||
                    (Math.abs(yFinal - yInit) <= 2 && premierCoup==true))) {

                ret = isSensVerticalOk(yInit, yFinal);
            }
        }
        // Déplacement diagonal
        else {
            ret = isDepDiagoOK(xInit, yInit, xFinal, yFinal);
        }

        return ret;
    }
    public abstract boolean isDepDiagoOK(int xInit, int yInit, int xFinal, int yFinal);

    public abstract boolean isSensVerticalOk(int yInit, int yFinal);
}
