package model.strategies;

public class FouMoveStrat implements MoveStrategy {
    private FouMoveStrat(){}
    private static FouMoveStrat INSTANCE = new FouMoveStrat();
    public static FouMoveStrat getINSTANCE(){
        return INSTANCE;
    }
    @Override
    public boolean isMoveOk(int xInit, int yInit, int xFinal, int yFinal, boolean isCatchOk, boolean isCastlingPossible, boolean premierCoup) {
        boolean ret = false;

        if (Math.abs(yFinal - yInit) == Math.abs(xFinal - xInit)) {
            ret  = true;
        }

        return ret;
    }
}
