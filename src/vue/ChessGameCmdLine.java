package vue;

import controler.ChessGameControlers;
import model.Coord;
//import controler.controlerLocal.ChessGameControler;
import model.Couleur;
import model.Echiquier;
import model.PieceIHMs;
import tools.MyObserver;

import java.util.Iterator;
import java.util.List;


/**
 * @author francoise.perrin
 * Inspiration Jacques SARAYDARYAN, Adrien GUENARD *
 * 
 * Vue console d'un jeu d'échec
 * Cette classe est un observateur et le damier est mis à jour à chaque changement dans la classe métier
 */
public class ChessGameCmdLine implements MyObserver {

	ChessGameControlers chessGameControler;

	public   ChessGameCmdLine(ChessGameControlers chessGameControler) {
		this.chessGameControler = chessGameControler;
	}


	public void go() {

		chessGameControler.move(new Coord(3,6), new Coord(3, 4));	// true
        System.out.print("\n Déplacement de 3,6 vers 3,4 = ");
        System.out.println(chessGameControler.getMessage() + "\n");
		System.out.println(chessGameControler);

		chessGameControler.move(new Coord(3,4), new Coord(3, 6));	// false
        System.out.print("\n Déplacement de 3,4 vers 3,6 = ");
        System.out.println(chessGameControler.getMessage() + "\n");
        System.out.println(chessGameControler);

		chessGameControler.move(new Coord(4, 1), new Coord(4, 3));	// true
        System.out.print("\n Déplacement de 4,1 vers 4,3 = ");
        System.out.println(chessGameControler.getMessage() + "\n");
		System.out.println(chessGameControler);

		chessGameControler.move(new Coord(3, 4), new Coord(3, 4));	// false
        System.out.print("\n Déplacement de 3,4 vers 3,4 = ");
        System.out.println(chessGameControler.getMessage() + "\n");
		System.out.println(chessGameControler);

		chessGameControler.move(new Coord(3, 4), new Coord(4, 3));	// true
        System.out.print("\n Déplacement de 3,4 vers 4,3 = ");
        System.out.println(chessGameControler.getMessage() + "\n");
		System.out.println(chessGameControler);
	}

	@Override
	public void update(List<PieceIHMs> pieceIHMs) {
		String[][] damier = new String[8][8];

		// création d'un tableau 2D avec les noms des pièces
		Iterator<PieceIHMs> pieceIHMsIterator = pieceIHMs.iterator();
		while (pieceIHMsIterator.hasNext()){
			PieceIHMs pieceIHM = pieceIHMsIterator.next();
			Couleur color = pieceIHM.getCouleur();
			String stColor = (Couleur.BLANC == color ? "B_" : "N_" );
			String type = (pieceIHM.getName()).substring(0, 2);


			damier[pieceIHM.getY()][pieceIHM.getX()] = stColor + type;

		}

		// Affichage du tableau formatté
		String st = "\n\n    0     1     2     3     4     5    6     7 \n";
		for ( int i = 0; i < 8; i++) {
			st += i + " ";
			for ( int j = 0; j < 8; j++) {
				String nomPiece = damier[i][j];
				if (nomPiece != null) {
					st += nomPiece + "  ";
				}
				else {
					st += "__  ";
				}
			}
			st +="\n";
		}

		System.out.println(st);
	}
}
